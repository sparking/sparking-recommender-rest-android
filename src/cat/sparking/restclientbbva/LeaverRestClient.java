package cat.sparking.restclientbbva;

import org.json.JSONException;
import org.json.JSONObject;

import cat.sparking.restclientbbva.models.Leaver;

public class LeaverRestClient extends RestClient{
	public LeaverRestClient() {
		super();
	}
	
	public Leaver create(Leaver arg) throws JSONException {
		return new Leaver (create(RestClientConstants.PARKEDS, arg).toString());
	}
	
	public Leaver retrieve(String id) throws JSONException {
		return new Leaver(retrieve(RestClientConstants.PARKEDS, id).toString());
	}
	
	public JSONObject update(String id, Leaver arg) throws JSONException {
		return update(RestClientConstants.PARKEDS, id, arg);
	}
	
	public Leaver patch(String id, Leaver arg) throws JSONException {
		return new Leaver(partialUpdate(RestClientConstants.PARKEDS, id, arg).toString());
	}
	
	public JSONObject destroy(String id) {
		return destroy(RestClientConstants.PARKEDS, id);
	}
	
	public JSONObject list(String what) {
		return list(what);
	}
}