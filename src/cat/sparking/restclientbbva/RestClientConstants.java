package cat.sparking.restclientbbva;

public class RestClientConstants {

	// API Root
	public final static String ROOT = "http://push-madlab.rhcloud.com/";

	// API Methods
	public final static String GET = "GET";
	public final static String POST = "POST";
	public final static String PUT = "PUT";
	public final static String PATCH = "PATCH";
	public final static String DELETE = "DELETE";

	// API Objects
	public final static String SEARCHERS = "searchers/";
	public final static String PARKEDS = "leavers/";
	
	// JSON Keys
	/*public final static String USERNAME = "username";
	public final static String PASSWORD = "password";
	public final static String COUNT = "count";
	public final static String RESULTS = "results";*/

	// Adhoc
	//public final static String CHECK_USER_CREDENTIALS = "check_user_credentials";

}
