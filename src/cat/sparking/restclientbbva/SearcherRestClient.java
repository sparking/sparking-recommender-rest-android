package cat.sparking.restclientbbva;

import org.json.JSONException;
import org.json.JSONObject;

import cat.sparking.restclientbbva.models.Searcher;

public class SearcherRestClient extends RestClient{
	public SearcherRestClient() {
		super();
	}
	
	public JSONObject create(Searcher arg) throws JSONException {
		return create(RestClientConstants.SEARCHERS, arg);
	}
	
	public Searcher retrieve(String id) throws JSONException {
		return new Searcher(retrieve(RestClientConstants.SEARCHERS, id).toString());
	}
	
	public JSONObject update(String id, Searcher arg) throws JSONException {
		return update(RestClientConstants.SEARCHERS, id, arg);
	}
	
	public JSONObject patch(String id, Searcher arg) throws JSONException {
		return partialUpdate(RestClientConstants.SEARCHERS, id, arg);
	}
	
	public JSONObject destroy(String id) {
		return destroy(RestClientConstants.SEARCHERS, id);
	}
	
	public JSONObject list(String what) {
		return list(what);
	}
}
