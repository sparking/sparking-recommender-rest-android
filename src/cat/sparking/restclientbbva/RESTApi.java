package cat.sparking.restclientbbva;

import org.json.JSONObject;

public interface RESTApi {
	
	public JSONObject list(String object);
	public JSONObject retrieve(String object, String id);
	public JSONObject create(String object, JSONObject args);
	public JSONObject update(String object, String id, JSONObject args);
	public JSONObject partialUpdate(String object, String id, JSONObject args);
	public JSONObject destroy(String object, String id);
}
