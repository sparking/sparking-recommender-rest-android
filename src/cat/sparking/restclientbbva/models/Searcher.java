package cat.sparking.restclientbbva.models;

import org.json.JSONException;
import org.json.JSONObject;

public class Searcher extends JSONObject {
	
	public Searcher() {
		super();
	}
	
	public Searcher(String searcher) throws JSONException {
		super(searcher);
	}
	
	public void putId(String value) {
		try {
			super.put("id", value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public int getId() {
		return super.optInt("id");
	}
	
	public void putGcmId(String value) {
		try {
			super.put("gcm_id", value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public String getGcmId() {
		return super.optString("gcm_id");
	}
	
	public void putLongitude(Double value) {
		try {
			super.put("lng", value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public double getLongitude() {
		return (Double) super.opt("lng");
	}
	
	public void putLatitude(Double value) {
		try {
			super.put("lat", value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public double getLatitude() {
		return (Double) super.opt("lat");
	}
	
	public void putRadius(Float value) {
		try {
			super.put("r", value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public double getRadius() {
		return (Double) super.opt("r");
	}
	
	public void putAction(String value) {
		try {
			super.put("action", value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public String getAction() {
		return super.optString("action");
	}
}
