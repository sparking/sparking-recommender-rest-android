package cat.sparking.restclientbbva.models;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SearcherResponse extends JSONObject{
	
	public SearcherResponse() {
		super();
	}
	
	public SearcherResponse(String value) throws JSONException {
		super(value);
		Log.i("MMM", value);
	}
	
	public ArrayList<Leaver> getParkeds() {
		ArrayList<Leaver> parkeds = new ArrayList<Leaver>();
		JSONObject parked_object = null;
		Iterator<String> i = null;
		try {
			if (!super.has("leavers")) {
				return null;
			}
			parked_object = super.getJSONObject("leavers");
			i = parked_object.keys();
			while (i.hasNext()) {
				String key = i.next();
				parkeds.add(new Leaver (parked_object.getString(key)));
			}
		} catch (JSONException e) {
			return null;
		}
		return parkeds;
	}
	
	public ArrayList<Searcher> getSearchers() {
		ArrayList<Searcher> searchers = new ArrayList<Searcher>();
		JSONObject searcher_object = null;
		Iterator<String> i = null;
		try {
			if (!super.has("searchers")) {
				return null;
			}
			searcher_object = super.getJSONObject("searchers");
			i = searcher_object.keys();
			while (i.hasNext()) {
				String key = i.next();
				searchers.add(new Searcher (searcher_object.getString(key)));
			}
		} catch (JSONException e) {
			return null;
		}
		return searchers;
	}
	
	public String getServerTime() {
		return super.optString("server_time");
	}
	
	public Searcher getSearcher() {
		try {
			return new Searcher(super.optString("searcher"));
		} catch (JSONException e) {
			return null;
		}
	}
}
