package cat.sparking.restclientbbva.models;

import org.json.JSONException;
import org.json.JSONObject;

public class Leaver extends JSONObject {
	
	public Leaver() {
		super();
	}
	
	public Leaver(String parked) throws JSONException {
		super(parked);
	}
	
	public void putId(String value) {
		try {
			super.put("id", value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public int getId() {
		return super.optInt("id");
	}
	
	public void putGcmId(String value) {
		try {
			super.put("gcm_id", value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public String getGcmId() {
		return super.optString("gcm_id");
	}
	
	public void putLongitude(Double value) {
		try {
			super.put("lng", value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public double getLongitude() {
		return (Double) super.opt("lng");
	}
	
	public void putLatitude(Double value) {
		try {
			super.put("lat", value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public double getLatitude() {
		return (Double) super.opt("lat");
	}
	
	public void putTimer(int value) {
		try {
			super.put("timer", value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public int getTimer() {
		return super.optInt("timer");
	}
	
	public void putParkTimer(Float value) {
		try {
			super.put("park_timer", value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public double getParkTimer() {
		return (Double) super.opt("park_timer");
	}
	
	public void putAction(String value) {
		try {
			super.put("action", value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public String getAction() {
		return super.optString("action");
	}
}
