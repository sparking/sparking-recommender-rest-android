package cat.sparking.restclientbbva;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Base64;

public class RestClientUtils {
	
	public static String basicAuthString(String uname, String pwd) {
		byte[] auth = (uname + ":" + pwd).getBytes();
		return "Basic " + Base64.encodeToString(auth, Base64.NO_WRAP);
	}
	
	public static JSONObject parseJSONResponse(InputStream inputStream) {
		return parseJSONResponse(convertInputStreamToString(inputStream));
	}

	public static JSONObject parseJSONResponse(String response) {
		try {
			return new JSONObject(response);
		} catch (JSONException e) {
			return null;
		}
	}

	public static String convertInputStreamToString(InputStream inputStream) {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		try {
			while ((line = bufferedReader.readLine()) != null) {
				result += line;
			}
			inputStream.close();
		} catch (IOException e) {
			return null;
		}
		return result;
	}

}
