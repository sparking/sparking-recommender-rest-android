package cat.sparking.restclientbbva;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class RestClient implements RESTApi {
		
		private String mUser = null;
		private String mPassword = null;
		private String mBasicAuth = null;
		
		public RestClient() {}
		
		public RestClient(String user, String password) {
			mUser = user;
			mPassword = password;
			mBasicAuth = RestClientUtils.basicAuthString(mUser, mPassword);
		}
		
		public void setCredentials(String user, String password) {
			mUser = user;
			mPassword = password;
			mBasicAuth = RestClientUtils.basicAuthString(mUser, mPassword);
		}
		
		public JSONObject list(String what) {
			JSONObject response = null;
			try {
				response = request(RestClientConstants.GET, what, null, null, mBasicAuth);
			} catch (IOException e) {
				try {
					response = new JSONObject(e.getLocalizedMessage());
				} catch (JSONException e1) {
					Log.e("RestClient", "Fatal error. " + e.getLocalizedMessage());
				}
			}
			return response;
		}
		
		public JSONObject retrieve(String what, String which) {
			JSONObject response = null;
			try {
				response = request(RestClientConstants.GET, what, which, null, mBasicAuth);
			} catch (IOException e) {
				try {
					response = new JSONObject(e.getLocalizedMessage());
				} catch (JSONException e1) {
					Log.e("RestClient", "Fatal error. " + e.getLocalizedMessage());
				}
			}
			return response;
		}
		
		public JSONObject create(String what, JSONObject args) {
			JSONObject response = null;
			try {
				response = request(RestClientConstants.POST, what, null, null, mBasicAuth, args);
			} catch (IOException e) {
				try {
					response = new JSONObject(e.getLocalizedMessage());
				} catch (JSONException e1) {
					Log.e("RestClient", "Fatal error. " + e.getLocalizedMessage());
				}
			}
			return response;
		}
		
		public JSONObject update(String what, String which, JSONObject args) {
			JSONObject response = null;
			try {
				response = request(RestClientConstants.PUT, what, which, null, mBasicAuth, args);
			} catch (IOException e) {
				try {
					response = new JSONObject(e.getLocalizedMessage());
				} catch (JSONException e1) {
					Log.e("RestClient", "Fatal error. " + e.getLocalizedMessage());
				}
			}
			return response;
		}
		
		public JSONObject partialUpdate(String what, String which, JSONObject args) {
			JSONObject response = null;
			try {
				response = request(RestClientConstants.PATCH, what, which, null, mBasicAuth, args);
			} catch (IOException e) {
				try {
					response = new JSONObject(e.getLocalizedMessage());
				} catch (JSONException e1) {
					Log.e("RestClient", "Fatal error. ", e);
				}
			}
			return response;
		}
		
		public JSONObject destroy(String what, String which) {
			JSONObject response = null;
			try {
				response = request(RestClientConstants.DELETE, what, which, null, mBasicAuth);
			} catch (IOException e) {
				try {
					response = new JSONObject(e.getLocalizedMessage());
				} catch (JSONException e1) {
					Log.e("RestClient", "Fatal error. " + e.getLocalizedMessage());
				}
			}
			return response;
		}

		private JSONObject request(String method, String object, String id, String function, String auth, JSONObject... args) throws MalformedURLException, IOException {
			
			int statusCode;

			// Form request url
			String url = RestClientConstants.ROOT + object + ((id != null) ? id + "/" : "") + ((function != null) ? function + "/" : "");
			Log.i("URL SEND", url);
			
			// Open connection
			HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
			
			// Set HTTP headers
			if (method.equals(RestClientConstants.PATCH)) {
				conn.setRequestMethod(RestClientConstants.POST);
			} else {
				conn.setRequestMethod(method);
			}
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Content-Type", "application/json");
			if (auth != null) {
				conn.setRequestProperty("Authorization", auth);
			}
			
			Log.i("RestClient", conn.getURL().toString());
			
			if (method.equals(RestClientConstants.GET)) {
				
				statusCode = conn.getResponseCode();
				if (statusCode != HttpURLConnection.HTTP_OK) {
					Log.e("RestClient", "Error Code " + statusCode);
					String errorString = RestClientUtils.convertInputStreamToString(conn.getErrorStream());
					throw new IOException(errorString);
				}
				return RestClientUtils.parseJSONResponse(conn.getInputStream());
				
			} else if (method.equals(RestClientConstants.POST)) {
				if (args == null) {
					throw new IOException("Missing input arguments.");
				}
				Log.i("RestClient", args[0].toString());
				conn.getOutputStream().write(args[0].toString().getBytes());
				statusCode = conn.getResponseCode();
				
				if ((statusCode & 200) != HttpURLConnection.HTTP_OK) {
					String errorString = RestClientUtils.convertInputStreamToString(conn.getErrorStream());
					Log.e("RestClient", "Error Code " + statusCode + ".\n" + errorString);
					throw new IOException(errorString);
				}
				return RestClientUtils.parseJSONResponse(conn.getInputStream());
				
			} else if (method.equals(RestClientConstants.PUT)) {
				
				if (args == null) {
					throw new IOException("Missing input arguments.");
				}
				Log.i("RestClient", "Sending: " + args[0].toString());
				conn.getOutputStream().write(args[0].toString().getBytes());
				statusCode = conn.getResponseCode();
				Log.i("AA", "Status code: " + statusCode);
				if (statusCode == HttpURLConnection.HTTP_NO_CONTENT) {
					return null;
				}
				if (statusCode != HttpURLConnection.HTTP_OK) {
					String errorString = RestClientUtils.convertInputStreamToString(conn.getErrorStream());
					throw new IOException(errorString);
				}
				return RestClientUtils.parseJSONResponse(conn.getInputStream());
				
			} else if (method.equals(RestClientConstants.PATCH)) {
				
				if (args == null) {
					throw new IOException("Missing input arguments.");
				}
				conn.setRequestProperty("X-HTTP-Method-Override", "PATCH");
				conn.getOutputStream().write(args[0].toString().getBytes());
				statusCode = conn.getResponseCode();
				if (statusCode != HttpURLConnection.HTTP_OK) {
					String errorString = RestClientUtils.convertInputStreamToString(conn.getErrorStream());
					Log.e("RestClient", "Error Code " + statusCode + ".\n" + errorString);
					throw new IOException(errorString);
				}
				return RestClientUtils.parseJSONResponse(conn.getInputStream());
				
			} else if (method.equals(RestClientConstants.DELETE)) {
				
				statusCode = conn.getResponseCode();
				if (statusCode != HttpURLConnection.HTTP_NO_CONTENT) {
					String errorString = RestClientUtils.convertInputStreamToString(conn.getErrorStream());
					throw new IOException(errorString);
				}
				return RestClientUtils.parseJSONResponse(conn.getInputStream());
				
			} else {
				throw new RestClientException("Method " + method + "does not exist.");
			}
		}
		
		public class RestClientException extends IOException {
			private static final long serialVersionUID = 1L;
			public RestClientException(String s) {
				super(s);
			}
		}
		
}
